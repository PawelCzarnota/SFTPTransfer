'Created and maintained by Pawel Czarnota
'This script does the following:
'1) maps O drive & P Drive
'2) copies all files to P Drive
'3) copies all relevant files to ... from O drive and deletes original files
'4) unmaps O drive and P drive
'5) Sends e-mail notifications to ... and ...

Const sSourceDRIVE = "O:"
'Const sSourceUNC = "\\networklocation\MY_TRANSFER"
Const sDestDRIVE = "P:"
Const sDestUNC = "\\networklocation\Loaded_MY_TRANSFER"
Set wshNetwork = CreateObject("WScript.Network") 
Dim email_message
Dim numberFiles
Dim FilesList
Dim confirmationFilesCount
Dim confirmationFilesList
Dim sendEmailTo
'set this variable to whomever should be getting SUCCESS emails from the application
sendEmailTo = ""
Dim sendEmailProductionSupportTo
'set this variable to whomever should be getting all emails from the application
sendEmailProductionSupportTo = ""

Wscript.Echo "------------------------------"
Wscript.Echo Date & " " & Time

Wscript.Echo "Attempting to create Source Drive"
On Error Resume Next
wshNetwork.MapNetworkDrive sSourceDRIVE, sSourceUNC
If Err.Number <> 0 Then
	email_message = "An error has occured when attempting to create Source Drive. Error# " & Err.Number & " " &Err.Description
	Wscript.Echo email_message
	Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
	Wscript.Quit Err.Number
End If
Wscript.Echo "Source Drive successfully created"

Wscript.Echo "Attempting to create Destination Drive"
wshNetwork.MapNetworkDrive sDestDRIVE, sDestUNC
If Err.Number <> 0 Then
	email_message = "An error has occured when attempting to create Destination Drive. Error# " & Err.Number & " " &Err.Description
	Wscript.Echo email_message
	Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
	'Unmap the Drives
	wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
	Wscript.Quit Err.Number
End If
Wscript.Echo "Destination Drive successfully created"

'make a list of all DAT files in Source drive and copy those files to a new directory on Destination drive
On Error Resume Next
Set fso = CreateObject("Scripting.FileSystemObject")
If Err.Number <> 0 Then
	email_message = "Error when creating FSO # " & Err.Number & " " & Err.Description
	Wscript.Echo email_message
	Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
	'Unmap the Drives
	wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
	wshNetwork.RemoveNetworkDrive sDestDRIVE, True
	Wscript.Quit Err.Number
End If

On Error Resume Next
Set sourceDirectory = fso.GetFolder(sSourceDRIVE)
If Err.Number <> 0 Then
	email_message = "Error trying to FSO.GetFolder - source # " & Err.Number & " " & Err.Description
	Wscript.Echo email_message
	Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
	'Unmap the Drives
	wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
	wshNetwork.RemoveNetworkDrive sDestDRIVE, True
	Wscript.Quit Err.Number
End If

On Error Resume Next
Set destinationDirectory = fso.GetFolder(sDestDRIVE)
If Err.Number <> 0 Then
	email_message = "Error trying to FSO.GetFolder - destination # " & Err.Number & " " & Err.Description
	Wscript.Echo email_message
	Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
	'Unmap the Drives
	wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
	wshNetwork.RemoveNetworkDrive sDestDRIVE, True
	Wscript.Quit Err.Number
End If

Set sourceFiles = sourceDirectory.Files
Set destinationFiles = destinationDirectory.Files
numberFiles = 0

'copy all DAT files to backup location
For Each sourceFile in sourceFiles
	If Right(sourceFile.Name,4) = ".dat" Then
		sFileName = sourceFile.Name
		'create new Directory
		newFolderName = destinationDirectory & Year(Date) & "-" & Month(Date) & "-" & Day(Date) & "\" 
		fso.CreateFolder(newFolderName)
		destinationFileName =  newFolderName & Replace(sFileName,".dat","") & "_" & Year(Date) & "-" & Month(Date) _
			& "-" & Day(Date) & "_" & Hour(Time) & "." & Minute(Time) & "." & Second(Time) & ".dat"
		'Wscript.Echo destinationFileName
		On Error Resume Next
		sourceFile.Copy destinationFileName, True 
		If Err.Number <> 0 Then
			email_message = "Error trying to copy files from Source to Destination # " & Err.Number & " " & Err.Description
			Wscript.Echo email_message
			Call Send_email_message(email_message, "ERROR",sendEmailProductionSupportTo)
			'Unmap the Drives
			wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
			wshNetwork.RemoveNetworkDrive sDestDRIVE, True
			Wscript.Quit Err.Number
		End If
		If numberFiles = 0 Then
				FilesList = vbCrlf & sFileName
		Else
				FilesList = FilesList & vbCrlf & sFileName
		End If
		numberFiles = numberFiles + 1
		Wscript.Echo "File copied: " & destinationFileName
	End If
Next		
Wscript.Echo "Number of DAT files: " & numberFiles

If numberFiles > 0 Then
	'transfer all files to SFTP server
	Wscript.Echo "Trying to transfer all files to SFTP server from Source drive"
	'On Error Resume Next
	ExecuteWithTerminalOutput("winscp.com /script=commands-MY_TRANSFER.txt")

	'check the results and send Success or Error message
	Set confirmationFiles = sourceDirectory.Files
	confirmationFilesList = ""
	confirmationFilesCount = 0
	For Each confirmationFile in confirmationFiles
		If Right(confirmationFile.Name,4) = ".dat" Then
			If confirmationFilesCount = 0 Then
				confirmationFilesList = vbCrlf & confirmationFile.Name
			Else
				confirmationFilesList = confirmationFilesList & vbCrlf & confirmationFile.Name
			End If
			confirmationFilesCount = confirmationFilesCount + 1
		End If
	Next
	
	If confirmationFilesCount = 0 Then
		'Send SUCCESS e-mail
		Wscript.Echo ""
		email_message = "SUCCESS: MY_TRANSFER task completed. Here is a list of all files that were transfered: " & FilesList
		Wscript.Echo email_message
		Call Send_email_message(email_message, "SUCCESS",sendEmailTo)
	Else
		'Send ERROR e-mail
		email_message = "ERROR: MY_TRANSFER task did not complete. Not all files have been transfered successfully. Here is a list of files that were not sent: " & confirmationFilesList
		Wscript.Echo email_message
		Call Send_email_message(email_message, "ERROR",sendEmailTo)
		'Unmap the Drives
		wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
		wshNetwork.RemoveNetworkDrive sDestDRIVE, True
		Wscript.Quit 100
	End If
Else
	email_message = "NO ACTION: MY_TRANSFER task completed. No files were found to be transferred."
	Wscript.Echo email_message
	Call Send_email_message(email_message, "NO ACTION",sendEmailProductionSupportTo)
End If

'Unmap the Drives
wshNetwork.RemoveNetworkDrive sSourceDRIVE, True
wshNetwork.RemoveNetworkDrive sDestDRIVE, True


'Function to run command
Sub ExecuteWithTerminalOutput(cmd)
Set sh = WScript.CreateObject("WScript.Shell")
On Error Resume Next
Set exec =  sh.Exec(cmd)
Do While exec.Status = 0
   WScript.Sleep 1
    WScript.StdOut.Write(exec.StdOut.ReadAll())
    WScript.StdErr.Write(exec.StdErr.ReadAll())
Loop
On Error Resume Next
ExecuteWithTerminalOutput = exec.Status

'Design Decision: Checking for return code is not reliable for this command. Instead number of files is compared
'If Err.Number <> 0 Then
'		email_message = "An error has occured while transfering files to ... . Error# " & Err.Number & " " &Err.Description
'		Wscript.Echo email_message
'		Call Send_email_message(email_message, "ERROR")
'		Wscript.Quit Err.Number
'End If
End Sub

'Function to send e-mail message using SMTP server
Sub Send_email_message(description, status,sendTo)
	Set objEmail = CreateObject("CDO.Message")
	objEmail.From = ""
	objEmail.To = sendTo
	objEmail.Subject = "Development - " & status & " MY_TRANSFER"
	objEmail.Textbody = description
	objEmail.Configuration.Fields.Item _
		("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	objEmail.Configuration.Fields.Item _
		("http://schemas.microsoft.com/cdo/configuration/smtpserver") = _
			"" 
	objEmail.Configuration.Fields.Item _
		("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 
	objEmail.Configuration.Fields.Update
	objEmail.Send
End Sub