option batch abort
option confirm off
open MY_TRANSFER
lcd O:
cd /home/destination/
put -delete -nopreservetime -nopermissions *.dat
exit